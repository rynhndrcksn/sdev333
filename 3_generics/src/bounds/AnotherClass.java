package bounds;

import java.awt.*;

/**
 * AnotherClass IS generic, AnotherClass is NOT a Color.
 * The types of the fields ARE Color objects
 * @param <T>
 */
public class AnotherClass<T extends Color> {
	// fields
	private T color;

	public AnotherClass(T color) {
		this.color = color;
	}

	public T getColor() {
		return color;
	}

	public void setColor(T color) {
		this.color = color;
	}
}
