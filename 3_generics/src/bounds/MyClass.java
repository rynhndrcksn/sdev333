package bounds;

import java.awt.*;

/**
 * MyClass is NOT generic, MyClass IS a Color
 */
public class MyClass extends Color {

	public MyClass(int red, int green, int blue) {
		super(red, green, blue);
	}
}
