package gardens;

public class Tester {
	public static void main(String[] args) {
		Garden<Plant> gardenWithPlant = new Garden<Plant>(new Plant("Green"));
		Garden<Flower> gardenWithFlower = new Garden<Flower>(new Flower("Purple"));
		Garden<Rose> gardenWithRose = new Garden<Rose>(new Rose("Red"));

		System.out.println(gardenWithPlant.getThing().getColor());
		System.out.println(gardenWithFlower.getThing().getColor());
		System.out.println(gardenWithRose.getThing().getColor());
	}
}
