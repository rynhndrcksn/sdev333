package practice;

/**
 * This class represents a triad of three Strings
 * @author Ryan H.
 * @version 1
 */
public class StringTriad extends Triad<String> {

	/**
	 * Default constructor
	 * @param first - String
	 * @param second - String
	 * @param third - String
	 */
	public StringTriad(String first, String second, String third) {
		super(first, second, third);
	}
}
