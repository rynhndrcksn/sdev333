package comparables.rectangles;

import java.util.Arrays;

public class TestRectangles
{
    private static Rectangle[] rects = {
        new Rectangle(10.2, 3.33),
        new Rectangle(9.99, 9.0),
        new Rectangle(1.7, 3.0),
        new Rectangle(12.3, 4.49),
        new Rectangle(1.111, 1.0)
    };

    public static void main(String[] args)
    {
        Arrays.sort(rects);
        for (Rectangle rect : rects) {
            System.out.println(rect);
        }
    }
}
