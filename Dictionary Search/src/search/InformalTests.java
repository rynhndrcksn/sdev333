package search;


/**
 * InformalTests is a class that tests the Node class as we go, will also test our DictionarySearch class
 * @author Ryan Hendrickson
 * @version 1.0
 */
public class InformalTests {
	/**
	 * entry point to InformalTests
	 * @param args - command line arguments
	 */
	public static void main(String[] args) {
		DictionarySearch dict = new DictionarySearch();
		System.out.println(dict.toString());
	}
}
