package search;

import java.util.ArrayList;
import java.util.List;

/**
 * Tree class that utilizes our Node class to create a tree
 * @author Ryan H.
 * @version 0.5
 */
public class Tree {
	// fields
	private Node<Character> root;
	private int size;

	/**
	 * adds a new word to our tree DS
	 * @param word - word to add to tree DS
	 */
	public void add(String word) {
		// check and see if we have a root already
		if (root == null) {
			root = new Node<>(null);
		}

		add(root, word);
	}

	/**
	 * helper method for public add(), this method allows us to recursively add each word
	 * @param node - node to see if the letter exists, and to modify the children of
	 * @param word - word to add to our data structure
	 */
	private void add(Node<Character> node, String word) {
		// find the first char
		char letter = word.charAt(0);

		// split the rest of the string for recursion
		String restOfWord = word.substring(1);

		// since root stays null, we need to start at the children, so we start at the index of the letter and attach it
		// to current
		Node<Character> current = node.getChildren()[node.getIndex(letter)];

		// if the letter isn't there, make it a new node containing the letter
		if (current == null) {
			// restOfWord contains letter
			if (!restOfWord.equals("")) {
				node.setChildren(new Node<>(letter));
			} else { // restOfWord is out of letters | BASE CASE
				// if the rest of the word is an empty string, it's safe to assume we're at a leaf node.
				node.setChildren(new Node<>(letter, true));
			}
			size++;
		}

		// now we move on down the tree!
		if (!restOfWord.equals("")) {
			if (node.isLeaf()) {
				node.setLeaf(false);
			}
			add(node.getChildren()[node.getIndex(letter)], restOfWord);
		} else {
			node.getChildren()[node.getIndex(letter)].setLeaf(true);
		}
	}

	/**
	 * public method to help find any/all partial matches entered in the dictionary
	 * @param word - String that contains the words
	 * @return - String array full of all our partial matches
	 */
	public String[] partialMatches(String word) {
		// If the search box isn't just letters and spaces, return null
		if (word.equals("") || word.equals(" ")) {
			return null;
		}

		// make our words array so we can return it at the end
		List<String> words = new ArrayList<>();

		// break our word up into a char array
		char[] letters = word.toCharArray();

		// we need a node for manipulating
		Node<Character> curr = root;

		// get to the bottom node
		for (char letter : letters) {
			curr = curr.getChildren()[curr.getIndex(letter)];
		}

		// pass our current node, the word, and our ArrayList into the search method
		search(curr, word, words);

		// return the ArrayList as an Array
		return words.toArray(new String[0]);
	}

	/**
	 * helped method for partialMatches
	 * @param node - node to start/continue search from
	 * @param word - partial word to use to find the others
	 * @param list - our ArrayList that gets passed in from the parent method
	 */
	private void search(Node<Character> node, String word, List<String> list) {
		// start getting our words!
		for (int i = 0; i < node.getChildren().length; i++) {
			// go through our nodes and see if they're not null. If they're not null we go to it!
			if (node.getChildren()[i] != null) {
				// make a new tempWord and concatenate our word and the letter together
				String tempWord = word + node.getChildren()[i].getData();
				// if we're not at a leaf, but we're at the end of a word...
				if (!node.getChildren()[i].isLeaf() && node.getChildren()[i].isEndOfWord()) {
					// we have a full word, so let's add it
					list.add(tempWord);
					// keep going down until we hit a leaf
					search(node.getChildren()[i], tempWord, list);
				} else if (node.getChildren()[i].isLeaf()) { // if we hit a leaf!
					// we add our word because we're at a leaf, nowhere to go from here
					list.add(tempWord);
				} else { // we need to keep moving down until we hit the end of a word or a leaf
					search(node.getChildren()[i], tempWord, list);
				}
			}
		}
	}

	/**
	 * @return root Node
	 */
	public Node<Character> getRoot() {
		return root;
	}

	/**
	 * @param root - set the root node
	 */
	public void setRoot(Node<Character> root) {
		this.root = root;
	}

	/**
	 * @return size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size - set the current size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Tree{" +
				"root=" + root +
				", size=" + size +
				'}';
	}
}
