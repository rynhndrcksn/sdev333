package flawedsolutions;

import sudoku.board.ISudokuBoard;
import sudoku.board.SudokuFileReader;
import sudoku.exceptions.InvalidBoardPositionException;

import java.util.*;

/**
 * Represents a 9x9 sudoku board. The elements in the
 * board are stored in a 9x9 array of integers where
 * 0 represents an empty location and 1-9 represent
 * a user choice.
 *
 * @author Josh Archer
 * @version 1.0
 */
public class SudokuFlawed3 implements ISudokuBoard
{
    public static final Random RANDOM = new Random();

    private SudokuFileReader randomBoards;
    private int[][] board;

    /**
     * Creates a new 9x9 sudoku board.
     */
    public SudokuFlawed3()
    {
        randomBoards = new SudokuFileReader();

        resetBoard();
    }

    private void resetBoard()
    {
        board = new int[BOARD_SIZE][BOARD_SIZE];
    }

    @Override
    public void populateBoard(int spots)
    {
        if (spots < 0 || spots > TOTAL_CELLS)
        {
            throw new IllegalArgumentException("Number of spots must be in the range [0,81]");
        }

        resetBoard();

        //pick a random board, then reveal the number of spots given
        int[] randomBoard = randomBoards.getRandomBoard();

        //copy over the solution
        for (int i = 0; i < randomBoard.length; i++)
        {
            assignCell(i, randomBoard[i]);
        }

        //obscure random cells on the board until only the number
        //of spots requested are showing
        int spotsToObscure = TOTAL_CELLS - spots;
        Set<Integer> obscured = new HashSet<>();
        while (obscured.size() < spotsToObscure)
        {
            int spot = RANDOM.nextInt(TOTAL_CELLS);
            obscured.add(spot);
            assignCell(spot, 0);
        }
    }

    private void assignCell(int index, int value)
    {
        int row = index / BOARD_SIZE;
        int column = index % BOARD_SIZE;

        board[row][column] = value;
    }

    @Override
    public void makeChoice(int choice, int row, int column)
    {
        checkIndex(row, IndexType.ROW);
        checkIndex(column, IndexType.COLUMN);

        if (choice < 1 || choice > BOARD_SIZE)
        {
            throw new IllegalArgumentException(choice + " must be in the range [1,9]");
        }

        board[0][column] = choice; //OOPS!
    }

    private void checkIndex(int value, IndexType index)
    {
        if (value < 0 || value >= BOARD_SIZE)
        {
            throw new InvalidBoardPositionException(index + " must be in the range [0,8].");
        }
    }

    @Override
    public int[][] getBoard()
    {
        int[][] result = new int[BOARD_SIZE][BOARD_SIZE];

        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                result[i][j] = board[i][j];
            }
        }

        return result;
    }

    @Override
    public boolean isRowValid(int row)
    {
        checkIndex(row, IndexType.ROW);

        Set<Integer> set = new HashSet<>();
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < BOARD_SIZE; i++)
        {
            countDuplicates(board[row][i], set, list);
        }

        return set.size() == list.size();
    }

    private void countDuplicates(int element, Set<Integer> set, List<Integer> list)
    {
        if (element != 0)
        {
            set.add(element);
            list.add(element);
        }
    }

    @Override
    public boolean isColumnValid(int column)
    {
        checkIndex(column, IndexType.COLUMN);

        Set<Integer> set = new HashSet<>();
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < BOARD_SIZE; i++)
        {
            countDuplicates(board[i][column], set, list);
        }

        return set.size() == list.size();
    }

    @Override
    public boolean isRegionValid(int region)
    {
        checkIndex(region, IndexType.REGION);

        int regionRow = (region / REGION_SIZE) * REGION_SIZE;
        int regionColumn = (region % REGION_SIZE) * REGION_SIZE;

        Set<Integer> set = new HashSet<>();
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < REGION_SIZE; i++)
        {
            for (int j = 0; j < REGION_SIZE; j++)
            {
                int element = board[regionRow + i][regionColumn + j];

                if (element != 0)
                {
                    set.add(element);
                    list.add(element);
                }
            }
        }
        return set.size() == list.size();
    }

    @Override
    public boolean isBoardFull()
    {
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            for (int j = 0; j < BOARD_SIZE; j++)
            {
                if (board[i][j] == 0)
                {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean isSolved()
    {
        if (!isBoardFull())
        {
            return false;
        }

        //check each row, column and region
        for (int i = 0; i < BOARD_SIZE; i++)
        {
            if (!isRegionValid(i) || !isRowValid(i) || !isColumnValid(i))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Board state: " + Arrays.toString(board);
    }

    private enum IndexType
    {
        ROW("Row"),
        COLUMN("Column"),
        REGION("Region");

        private String output;

        IndexType(String output)
        {
            this.output = output;
        }

        @Override
        public String toString()
        {
            return output;
        }
    }
}
