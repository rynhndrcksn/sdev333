package filereader;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author Ryan H.
 * @version 1.0
 * creates an iterable Scanner for files
 */
public class FileReader implements Iterable<String> {
	private String path;

	/**
	 * @param path - the path to our file
	 */
	public FileReader(String path) {
		this.path = path;
	}

	@Override
	public Iterator<String> iterator() {
		return new FileIterator();
	}

	private class FileIterator implements Iterator<String> {
		private Scanner reader;

		public FileIterator() {
			try {
				reader = new Scanner(new FileInputStream(path));
			} catch (FileNotFoundException e) {
				// I want the program to terminate if I can't open the file
				throw new IllegalStateException("Error: Cant access file...");
			}
		}

		@Override
		public boolean hasNext() {
			// iterator will query the scanner to see if there's an additional line
			return reader.hasNextLine();
		}

		@Override
		public String next() {
			// iterator will use scanner to read the next line
			return reader.nextLine();
		}

		@Override
		public String toString() {
			return "FileIterator{" +
					"reader=" + reader +
					'}';
		}
	}

	@Override
	public String toString() {
		return "FileReader{" +
				"path='" + path + '\'' +
				'}';
	}
}
