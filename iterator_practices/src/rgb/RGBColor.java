package rgb;

import java.util.Iterator;

/**
 * @author Ryan H.
 * @version 1.0
 * creates a color using RGB
 */
public class RGBColor implements Iterable<Integer> {
	private int red;
	private int green;
	private int blue;
	private int[] colors;

	/**
	 * @param red - red value
	 * @param green - green value
	 * @param blue - blue value
	 */
	public RGBColor(int red, int green, int blue) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		colors = new int[]{red, green, blue};
	}

	/**
	 * @return red
	 */
	public int getRed() {
		return red;
	}

	/**
	 * @param red - value of red
	 */
	public void setRed(int red) {
		this.red = red;
	}

	/**
	 * @return green
	 */
	public int getGreen() {
		return green;
	}

	/**
	 * @param green  - value of green
	 */
	public void setGreen(int green) {
		this.green = green;
	}

	/**
	 * @return blue
	 */
	public int getBlue() {
		return blue;
	}

	/**
	 * @param blue - value of blue
	 */
	public void setBlue(int blue) {
		this.blue = blue;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new RGBIterator();
	}

	private class RGBIterator implements Iterator<Integer> {
		// index of the next element to return from the iterator!
		private int current;

		public RGBIterator() {
			current = 0;
		}

		@Override
		public boolean hasNext() {
			// return true if the current is less than all the length of colors array
			return current < colors.length;
		}

		@Override
		public Integer next() {
			// return the next element in the iteration and prepare for the element after that one
			return colors[current++];
		}

		@Override
		public String toString() {
			return "RGBIterator{" +
					"current=" + current +
					'}';
		}
	}

	@Override
	public String toString() {
		return "RGBColor{" +
				"red=" + red +
				", green=" + green +
				", blue=" + blue +
				'}';
	}
}
