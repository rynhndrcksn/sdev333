package classes;

public class CorgiPuppy extends Puppy{

	public CorgiPuppy(String name, String nickname, int monthsOld, double weightLbs) {
		super(name, nickname, monthsOld, weightLbs);
	}

	@Override
	public boolean equals(Object other) {
		// make sure we have a puppy
		if (other == null || !(other instanceof CorgiPuppy)) {
			return false;
		}

		// we have a puppy!
		CorgiPuppy corgi = (CorgiPuppy) other;

		// if their names are equal, they're puppies
		return corgi.getName().equals(getName());
	}
}
