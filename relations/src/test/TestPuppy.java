package test;

import classes.Puppy;

public class TestPuppy {
	public static void main(String[] args) {
		Puppy pup1 = new Puppy("Chewy", "Chew Toy", 10, 25.3);
		Puppy pup2 = new Puppy("Mr. Darcy", "Mister", 4, 3.2);
		Puppy pup3 = new Puppy("Toby", "Tobenator", 20, 22.0);

		System.out.println("pup1 & pup2 the same? " + pup1.equals(pup2));
	}
}
