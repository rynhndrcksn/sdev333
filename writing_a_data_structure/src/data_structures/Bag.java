package data_structures;

import interfaces.Collection;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/**
 * Bag class
 * @author Ryan Hendrickson
 * @version .5
 */
public class Bag<T> implements Collection<T>, Iterable<T> {
	// fields
	private T[] data;
	// hold the next available index
	private int nextIndex;

	// this counter keeps track of the number of changes in the structure
	private int modCount = 0;

	/**
	 * default/only constructor
	 * @param capacity - how big the user wants the bag to be
	 */
	public Bag(int capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException("Capacity must be a positive value.");
		}
		data = (T[]) new Object[capacity];
	}

	/**
	 * adds a new element to the bag
	 * @param element is the element the user wants to add
	 * @return false if bag is full, true if the element was added
	 */
	@Override
	public boolean add(T element) {
		// if nextIndex == data.length we know our Bag is full
		if (nextIndex == data.length) {
			return false;
		} else {
			data[nextIndex++] = element;
			modCount++;
			return true;
		}
	}

	/**
	 * check to see if our bag contains the element
	 * @param element to check if it's in the bag
	 * @return true if the bag contains the element, false if it doesn't
	 */
	@Override
	public boolean contains(T element) {
		for (int i = 0; i < nextIndex; i++) {
			if (data[i].equals(element)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * gives us the size of the bag
	 * @return nextIndex, which tells us how much stuff is in the bag
	 */
	@Override
	public int size() {
		return nextIndex;
	}

	@Override
	public boolean isEmpty() {
		return nextIndex == 0;
	}

	@Override
	public boolean remove(T element) {
		// if our array is empty, we can't remove anything
		if (nextIndex != 0) {
			for (int i = 0; i < nextIndex; i++) {
				if (data[i].equals(element)) {
					int current = i;
					int next = current + 1;
					while (data[current] != null) {
						data[current] = data[next];
						current++;
						next++;
					}
					nextIndex--;
					modCount++;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param element - thing to remove
	 * @return true if thing is removed, false otherwise
	 */
	public boolean removeJosh(Object element) {
		// loop until the last used index
		for (int i = 0; i < nextIndex; i++) {
			if (data[i].equals(element)) {
				// move all the elements down 1 index | data.length - 1 helps ensure we stay in bounds
				for (int j = i; j < nextIndex && j < data.length - 1; j++) {
					data[j] = data[j+1];
				}
				// drop the last element, move the nextIndex down, and return true
				data[nextIndex - 1] = null;
				nextIndex--;
				return true;
			}
		}
		return false;
	}

	@Override
	public void clear() {
		for (int i = 0; i < nextIndex; i++) {
			data[i] = null;
		}
		nextIndex = 0;
		modCount++;
	}

	@Override
	public Iterator<T> iterator() {
		return new BagIterator(modCount);
	}

	@Override
	public boolean addAll(Collection<T> other) {
		throw new UnsupportedOperationException("Operation not supported...");
	}

	@Override
	public boolean containsAll(Collection<T> other) {
		throw new UnsupportedOperationException("Operation not supported...");
	}

	@Override
	public boolean removeAll(Collection<T> other) {
		throw new UnsupportedOperationException("Operation not supported...");
	}

	@Override
	public boolean retainAll(Collection<T> other) {
		throw new UnsupportedOperationException("Operation not supported...");
	}

	@Override
	public int hashcode() {
		throw new UnsupportedOperationException("Operation not supported...");
	}


	@Override
	public T[] toArray() {
		throw new UnsupportedOperationException("Operation not supported...");
	}

	// new classes always go at the bottom, our <T> is the same <T> from the top where: Bag<T>
	// BagIterator will also iterate through what's in the bag, not multiple bags
	private class BagIterator implements Iterator<T> {
		// index of the next element to return from the iterator!
		private int current;
		private int savedModCount;

		public BagIterator(int modCount) {
			current = 0;
			savedModCount = modCount;
		}

		@Override
		public boolean hasNext() {
			checkConcurrentChanges();
			// return true if the current is less than all the elements in the bag, and it isn't null
			return current < data.length && data[current] != null;
		}

		@Override
		public T next() {
			checkConcurrentChanges();
			// return the next element in the iteration and prepare for the element after that one
			return data[current++];
		}

		private void checkConcurrentChanges() {
			// if modified count has deviated
			if (modCount != savedModCount) {
				throw new ConcurrentModificationException("You cannot change the structure while iterating...");
			}
		}

		@Override
		public String toString() {
			return "BagIterator{" +
					"current=" + current +
					'}';
		}
	}

	@Override
	public String toString() {
		return "Bag{" +
				"data=" + Arrays.toString(data) +
				", nextIndex=" + nextIndex +
				'}';
	}
}
